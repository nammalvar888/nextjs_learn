import Link from "next/link";
import { useEffect } from "react";
import { useRouter } from "next/router";

const NotFound = () => {
  const redirect = useRouter();
  useEffect(() => {
    setTimeout(() => {
      redirect.push("/");
    }, 1200);
  }),
    [];

  return (
    <div>
      <h1>sorry page is found 😢</h1>
      <Link href="/">Go to Home Page</Link>
    </div>
  );
};

export default NotFound;
