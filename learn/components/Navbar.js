import Link from "next/link";

const Navbar = () => {
  return (
    <>
      <div>Navbar</div>
      <Link href="/">
        <h1>Go Home</h1>
      </Link>
      <Link href="/about">
        <h1>Go About</h1>
      </Link>
    </>
  );
};

export default Navbar;
